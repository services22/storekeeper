//
//  User.swift
//  App
//
//  Created by Imthath M on 13/05/19.
//

import Vapor

final class User: VaporType {
    var id: UUID?
    var email: String
    var hashedPassword: String
    
    public init(email: String, password: String) {
        self.email = email
        self.hashedPassword = password
    }
}
