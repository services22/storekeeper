//
//  Order.swift
//  App
//
//  Created by Imthath M on 19/05/19.
//

import Vapor
import FluentPostgreSQL

final class Order: VaporType {
    var id: UUID?
    var number: Int
    var shopId: Shop.ID
    var placeId: Place.ID
    
    var shop: Parent<Order, Shop> {
        return parent(\.shopId)
    }
    
    var place: Parent<Order, Place> {
        return parent(\.shopId)
    }
    
    var orderItems: Siblings<Order, Item, OrderItem> {
        return siblings()
    }
    
    public init(id: UUID? = nil, shopId: Shop.ID, placeId: Place.ID) {
        self.id = id
        self.number = Int(Date().timeIntervalSince(Date.jan2019))
        self.shopId = shopId
        self.placeId = placeId
    }
}

extension Order {
    struct Full: Content {
        var id: UUID?
        var number: Int
        var shopId: Shop.ID
        var placeId: Place.ID
        var items: [Item.Info]?
        var itemsCost: Double
        var taxAmount: Double
        var totalCost: Double
        
        public init(_ order: Order, items: [Item.Info]? = nil, itemsCost: Double, taxAmount: Double, totalCost: Double) {
            self.id = order.id
            self.number = order.number
            self.shopId = order.shopId
            self.placeId = order.placeId
            self.items = items
            self.itemsCost = itemsCost
            self.taxAmount = taxAmount
            self.totalCost = totalCost
        }
    }
    
    private func getItemsCost(on conn: DatabaseConnectable) throws -> Future<Double> {
        return OrderItem.query(on: conn)
            .join(\Item.id, to: \OrderItem.itemId)
            .filter(\OrderItem.orderId == self.id!)
            .alsoDecode(Item.self).all().map { orders -> Double in
                return orders.map { self.getCost($0.0, $0.1) }.reduce(0, +)
        }
    }
    
    private func getCost(_ orderItem: OrderItem, _ item: Item) -> Double {
        return item.price * Double(orderItem.quantity)
    }
    
    public func findTax(for cost: Double, on conn: DatabaseConnectable) throws -> Future<Double> {
        return Shop.query(on: conn)
            .filter(\.id == self.shopId).first().map { shop -> Double in
                guard let existingShop = shop else {
                    throw CustomError(identifier: ErrorId.shop, reason: ErrorReason.notFound)
                }
                
                return (cost * Double(existingShop.taxPercentage))/100
        }
    }
    
    public func totalCost(on conn: DatabaseConnectable) throws -> Future<Full> {
        return try getItemsCost(on: conn).flatMap { cost in
            return try self.findTax(for: cost, on: conn).map { tax in
                return Full(self, itemsCost: cost, taxAmount: tax, totalCost: cost + tax)
            }
        }
    }
}

final class OrderItem: Pivot, VaporType {
    
    typealias Left = Order
    typealias Right = Item
    
    static var leftIDKey: WritableKeyPath<OrderItem, UUID> = \.orderId
    static var rightIDKey: WritableKeyPath<OrderItem, UUID> = \.itemId
    
    var id: UUID?
    var orderId: Order.ID
    var itemId: Item.ID
    var shopId: Shop.ID
    var quantity: Int
    
    public init(id: UUID? = nil, orderId: Order.ID, itemId: Item.ID,
                shopId: Shop.ID, quantity: Int) {
        self.id = id
        self.orderId = orderId
        self.itemId = itemId
        self.shopId = shopId
        self.quantity = quantity
    }
}
