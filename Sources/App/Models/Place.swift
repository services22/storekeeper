//
//  Place.swift
//  App
//
//  Created by Imthath M on 18/05/19.
//

import Vapor
import FluentPostgreSQL

final class Place: VaporType {
    var id: UUID?
    var shopId: Shop.ID
    var number: Int
    var capacity: Int
    var type: PlaceType
//    var isAvailable: Bool
    
    var shop: Parent<Place, Shop> {
        return parent(\.shopId)
    }
    
    var orders: Children<Place, Order> {
        return children(\.placeId)
    }
    
    public init(shopId: Shop.ID, number: Int, capacity: Int, type: PlaceType) {
        self.shopId = shopId
        self.number = number
        self.capacity = capacity
        self.type = type
    }
}

public enum PlaceType: Int, Codable {
    case table
    case room
}

//struct PlaceAddCapacityMigration: PostgreSQLMigration {
//
//    static func prepare(on conn: PostgreSQLConnection) -> EventLoopFuture<Void> {
//        return Database.update(Place.self, on: conn) { builder in
//            builder.field(for: \.capacity, type: .int, PostgreSQLColumnConstraint.default(4))
//        }
//    }
//
//    static func revert(on conn: PostgreSQLConnection) -> EventLoopFuture<Void> {
//        return conn.future()
//    }
//}



