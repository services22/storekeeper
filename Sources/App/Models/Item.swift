//
//  Item.swift
//  App
//
//  Created by Imthath M on 22/05/19.
//

import Vapor
import FluentPostgreSQL

final class Item: VaporType {
    var id: UUID?
    var shopId: Shop.ID
    var categoryId: Category.ID
    var name: String
    var imageLink: URL?
    var isAvailable: Bool
    var price: Double
    var code: String?
    
    var shop: Parent<Item, Shop> {
        return parent(\.shopId)
    }
    
    var category: Parent<Item, Category> {
        return parent(\.categoryId)
    }
    
    var orders: Siblings<Item, Order, OrderItem> {
        return siblings()
    }
}

extension Item: Hashable {
    static func == (lhs: Item, rhs: Item) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

extension Array where Element: Item {
    func uniqueCount() -> [Item.Info] {
        var result = [Element: Int]()
        
        for element in self {
            if let count = result[element] {
                result[element] = count + 1
            } else {
                result[element] = 1
            }
        }
        
        var items = [Item.Info]()
        for (item, quantity) in result {
            items.append(Item.Info(id: item.id, name: item.name, quantity: quantity,
                                   unitPrice: item.price))
        }
        
        return items
    }
}

extension Item {
    final class Info: Content {
        var id: Item.ID?
        var name: String
        var quantity: Int
        var unitPrice: Double
        
        public init(id: Item.ID? = nil, name: String, quantity: Int, unitPrice: Double) {
            self.id = id
            self.name = name
            self.quantity = quantity
            self.unitPrice = unitPrice
        }
    }
}

extension Array where Element: Item.Info {
    var itemsCost: Double {
        return self.map { $0.unitPrice * Double($0.quantity) }.reduce(0, +)
    }
    
    func tax(for shop: Shop) -> Double {
        return (Double(shop.taxPercentage) * itemsCost)/100
    }
    
    func totalCost(on shop: Shop) -> Double {
        return itemsCost + tax(for: shop)
    }
}

//struct ItemAddAvailability: PostgreSQLMigration {
//    static func prepare(on conn: PostgreSQLConnection) -> EventLoopFuture<Void> {
//        return Database.update(Item.self, on: conn) { builder in
//            builder.field(for: \.isAvailable, type: .bool, PostgreSQLColumnConstraint.default(.literal("true")))
//        }
//    }
//    
//    static func revert(on conn: PostgreSQLConnection) -> EventLoopFuture<Void> {
//        return conn.future()
//    }
//    
//    
//}
