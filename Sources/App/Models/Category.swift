//
//  Category.swift
//  App
//
//  Created by Imthath M on 25/07/19.
//

import Vapor
import FluentPostgreSQL

final class Category: VaporType {
    var id: UUID?
    var shopId: Shop.ID
    var name: String
    var imageLink: URL?
    
    var shop: Parent<Category, Shop> {
        return parent(\.shopId)
    }
    
    var items: Children<Category, Item> {
        return children(\.categoryId)
    }
}
