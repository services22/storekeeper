//
//  Offer.swift
//  App
//
//  Created by Imthath M on 25/07/19.
//

import Vapor

final class Offer: VaporType {
    var id: UUID?
    var name: String
    var code: String
    var imageLink: URL
    var percentage: Int
    var startDate: Date
    var endDate: Date
    var minimumOrderAmount: Int
    var maximumDiscountAmount: Int
}
