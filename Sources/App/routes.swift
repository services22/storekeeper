import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    try router.register(collection: ShopRoutes(Path.shops))
    try router.register(collection: PlaceRoutes(Path.shopPlaces))
    try router.register(collection: OrderRoutes(Path.shopOrders))
    try router.register(collection: CategoryRoutes(Path.shopCategories))
    try router.register(collection: ItemRoutes())
    
    try router.register(collection: VaporRoutes<Shop>(Path.shops))
    try router.register(collection: VaporRoutes<Place>(Path.places))
    try router.register(collection: VaporRoutes<Category>(Path.categories))
    try router.register(collection: VaporRoutes<Item>(Path.items))
}
