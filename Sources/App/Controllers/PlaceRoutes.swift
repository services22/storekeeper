//
//  PlaceRoutes.swift
//  App
//
//  Created by Imthath M on 17/05/19.
//

import Vapor

final class PlaceRoutes: RouteCollection {
    
    let paths: PathComponentsRepresentable
    
    public init(_ paths: PathComponentsRepresentable) {
        self.paths = paths
    }
    
    func boot(router: Router) throws {
        let routes = router.grouped(paths)
        
        routes.get(use: list)
        routes.get(Place.parameter, use: show)
        routes.post(AddPlaces.self, use: addPlaces)
        routes.delete(Place.parameter, use: delete)
    }
    
    func list(_ request: Request) throws -> Future<[Place]> {
        return try request.parameters.next(Shop.self).flatMap { shop in
            return try shop.places.query(on: request).all()
        }
    }
    
    func addPlaces(_ request: Request, _ addPlaces: AddPlaces) throws -> Future<[Place]> {
        return try request.parameters.next(Shop.self).flatMap { shop in
            var places = [Place]()
            let upToNumber = addPlaces.fromNumber + addPlaces.count
            for number in addPlaces.fromNumber..<upToNumber {
                places.append(Place(shopId: shop.id!, number: number,
                                    capacity: addPlaces.capacity, type: addPlaces.type))
            }
            return places.map { $0.save(on: request) }.flatten(on: request)
        }
    }
    
    func show(_ request: Request) throws -> Future<Place> {
        return try request.parameters.next(Place.self)
    }
    
    func delete(_ request: Request) throws -> Future<HTTPStatus> {
        return try request.parameters.next(Place.self)
            .delete(on: request)
            .transform(to: .ok)
    }
}

struct AddPlaces: Content {
    var fromNumber: Int
    var count: Int
    var capacity: Int
    var type: PlaceType
}
