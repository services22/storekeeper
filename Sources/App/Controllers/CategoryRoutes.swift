//
//  CategoryRoutes.swift
//  App
//
//  Created by Imthath M on 19/05/19.
//

import Vapor

struct CategoryRoutes: RouteCollection {
    
    let paths: PathComponentsRepresentable
    
    public init(_ paths: PathComponentsRepresentable) {
        self.paths = paths
    }
    
    func boot(router: Router) throws {
        let routes = router.grouped(paths)
        
        routes.get(use: list)
    }
    
    func list(_ request: Request) throws -> Future<[Category]> {
        return try request.parameters.next(Shop.self).flatMap { shop in
            return try shop.categories.query(on: request).all()
        }
    }
}
