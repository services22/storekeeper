//
//  ShopRoutes.swift
//  App
//
//  Created by Imthath M on 17/05/19.
//

import Vapor

final class ShopRoutes: RouteCollection {
    
    let paths: PathComponentsRepresentable
    
    public init(_ paths: PathComponentsRepresentable) {
        self.paths = paths
    }
    
    func boot(router: Router) throws {
        let routes = router.grouped(paths)
        
        routes.get(Path.places, use: getPlaces)
        routes.get(Path.categories, use: getCategories)
        routes.get(Path.items, use: getItems)
        routes.get(Shop.parameter, use: show)
        routes.delete(Shop.parameter, use: delete)
    }
    
    func show(_ request: Request) throws -> Future<Shop> {
        return try request.parameters.next(Shop.self)
    }
    
    func delete(_ request: Request) throws -> Future<HTTPStatus> {
        return try request.parameters.next(Shop.self)
            .delete(on: request)
            .transform(to: .ok)
    }
    
    func getPlaces(_ request: Request) throws -> Future<[Shop.Full]> {
        return Shop.query(on: request).all().flatMap { shops in
            return try shops.map { try $0.places(on: request) }.flatten(on: request)
        }
    }
    
    func getCategories(_ request: Request) throws -> Future<[Shop.Full]> {
        return Shop.query(on: request).all().flatMap { shops in
            return try shops.map { try $0.categories(on: request) }.flatten(on: request)
        }
    }
    
    func getItems(_ request: Request) throws -> Future<[Shop.Full]> {
        return Shop.query(on: request).all().flatMap { shops in
            return try shops.map { try $0.items(on: request) }.flatten(on: request)
        }
    }
}
