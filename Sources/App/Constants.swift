//
//  Constants.swift
//  App
//
//  Created by Imthath M on 17/05/19.
//
import Vapor

struct Path {
    static let shops = "shops"
    static let full = "full"
    static let places = "places"
    static let shopPlaces: PathComponentsRepresentable = [shops, Shop.parameter, places]
    static let categories = "categories"
    static let shopCategories: PathComponentsRepresentable = [shops, Shop.parameter, categories]
    static let items = "items"
    static let categoryItems: PathComponentsRepresentable = [categories, Category.parameter, items]
    static let shopItems: PathComponentsRepresentable = [shops, Shop.parameter, items]
    static let orders = "orders"
    static let shopOrders: PathComponentsRepresentable = [shops, Shop.parameter, orders]
    static let orderItems: PathComponentsRepresentable = [orders, Order.parameter, items]
    static let changeItemAvailability: PathComponentsRepresentable = [items, Item.parameter, "changeAvailability"]
}

extension Date {
    static let jan2019 = Date(timeIntervalSince1970: 1546300800)
}


struct CustomError: Debuggable {
    var identifier: String
    var reason: String
}

struct ErrorId {
    static let order = "Order"
    static let shop = "Shop"
}

struct ErrorReason {
    static let notFound = "Given id not found in database."
}
