import Vapor
import FluentPostgreSQL

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    // Register providers first
    try services.register(FluentPostgreSQLProvider())

    // Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    // Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    // middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)

    // Configure a PostgreSQL database
    let postgres: PostgreSQLDatabase
    
    if let url = Environment.get("DATABASE_URL"),
        let dbConfig = PostgreSQLDatabaseConfig(url: url) {
        postgres = PostgreSQLDatabase(config: dbConfig)
    } else {
        let dbConfig = PostgreSQLDatabaseConfig(hostname: "localhost",
                                                username: "mohammed-7418",
                                                database: "FooodService")
        postgres = PostgreSQLDatabase(config: dbConfig)
    }

    // Register the configured SQLite database to the database config.
    var databases = DatabasesConfig()
    databases.add(database: postgres, as: .psql)
    services.register(databases)

    // Configure migrations
    var migrations = MigrationConfig()
    migrations.add(model: Shop.self, database: .psql)
    migrations.add(model: Place.self, database: .psql)
    migrations.add(model: Category.self, database: .psql)
    migrations.add(model: Item.self, database: .psql)
    migrations.add(model: Order.self, database: .psql)
    migrations.add(model: OrderItem.self, database: .psql)
//    migrations.add(migration: ItemAddAvailability.self, database: .psql)
    services.register(migrations)
}
